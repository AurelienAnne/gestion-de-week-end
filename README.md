Aurélien ANNE, Kevin SALMON
La dernière version stable du projet est déployée sur [https://glitaa.kevinsalmon.fr/](https://glitaa.kevinsalmon.fr/).

# Lancement du projet en mode production avec docker
* Compilation d'un .war en mode production et création d'une image docker : _sudo ./mvnw package -DskipTests=true -Pprod dockerfile:build_
* Lancement de l'image docker : _sudo docker-compose -f src/main/docker/app.yml up_
* L'application est disponible sur [http://localhost:8095](http://localhost:8095). Compte administrateur / Mdp : _admin_ / _admin_. Lors de l'inscription au site, l'envoi du mail d'activation peut prendre 5 minutes et il peut aller dans le dossier Spam. Le chargement de l'ensemble des données depuis OpenWeatherMap s'effectue du mercredi au vendredi à 1h. Un mail est ensuite envoyé à chaque utilisateur avec sa liste de lieux de prédilection et le score météo.
* La base de données et le cache des données OpenWeatherMap sont montés dans le dossier ~/projetglitaa

# Lancement du projet en mode développement avec docker
* Cloner le projet et exécuter la commande _yarn install_.
* Lancer la compilation d'angular avec la commande _yarn start_.
* Ouvrir le projet dans eclipse en tant que projet Maven.
* Dans eclipse, lancer en tant qu'application Java le fichier _projetglitaa > src/main/java > glitaa > ProjetglitaaApp.java_
* L'application est disponible sur [http://localhost:8080](http://localhost:8080). Le chargement de l'ensemble des données depuis OpenWeatherMap s'effectue du mercredi au vendredi à 1h. Les données recueillies sont disponibles dans le dossier _./cache/Weather_. Pour tester l'application sans attendre le chargemnt automatique, un fichier compressé .tar.gz contenant des données d'OpenWeatherMap est disponible dans le dossier _./ressources_. Il suffit de l'extraire dans _./cache/Weather_. La base de données est hébergée à l'ISTIC sur :
    * URL : mysql.istic.univ-rennes1.fr:3306
    * BDD : base_16001615
    * Utilisateur : user_16001615
    * Mot de passe : 123456


# Commandes Docker générales
* Accès à un container : _sudo docker exec -i -t <DébutDeL'ID> /bin/bash_
* Voir les containers lancés : _sudo docker ps -a_
* Supprimer un container : _sudo docker rm <DébutDeL'ID>_
* Voir les images disponibles : _sudo docker images_
* Supprimer une image : _sudo docker rmi <DébutDeL'ID>_




# projetglitaa
This application was generated using JHipster 4.8.2, you can find documentation and help at [http://www.jhipster.tech/documentation-archive/v4.8.2](http://www.jhipster.tech/documentation-archive/v4.8.2).

## Building for production

To optimize the projetglitaa application for production, run:

    ./mvnw -Pprod clean package

This will concatenate and minify the client CSS and JavaScript files. It will also modify `index.html` so it references these new files.
To ensure everything worked, run:

    java -jar target/*.war

Then navigate to [http://localhost:8080](http://localhost:8080) in your browser.

Refer to [Using JHipster in production][] for more details.

## Testing

To launch your application's tests, run:

    ./mvnw clean test

### Client tests

Unit tests are run by [Karma][] and written with [Jasmine][]. They're located in [src/test/javascript/](src/test/javascript/) and can be run with:

    yarn test



For more information, refer to the [Running tests page][].

## Using Docker to simplify development (optional)

You can use Docker to improve your JHipster development experience. A number of docker-compose configuration are available in the [src/main/docker](src/main/docker) folder to launch required third party services.
For example, to start a mysql database in a docker container, run:

    docker-compose -f src/main/docker/mysql.yml up -d

To stop it and remove the container, run:

    docker-compose -f src/main/docker/mysql.yml down

You can also fully dockerize your application and all the services that it depends on.
To achieve this, first build a docker image of your app by running:

    ./mvnw package -Pprod dockerfile:build

Then run:

    docker-compose -f src/main/docker/app.yml up -d

For more information refer to [Using Docker and Docker-Compose][], this page also contains information on the docker-compose sub-generator (`jhipster docker-compose`), which is able to generate docker configurations for one or several JHipster applications.

## Continuous Integration (optional)

To configure CI for your project, run the ci-cd sub-generator (`jhipster ci-cd`), this will let you generate configuration files for a number of Continuous Integration systems. Consult the [Setting up Continuous Integration][] page for more information.

[JHipster Homepage and latest documentation]: http://www.jhipster.tech
[JHipster 4.8.2 archive]: http://www.jhipster.tech/documentation-archive/v4.8.2

[Using JHipster in development]: http://www.jhipster.tech/documentation-archive/v4.8.2/development/
[Using Docker and Docker-Compose]: http://www.jhipster.tech/documentation-archive/v4.8.2/docker-compose
[Using JHipster in production]: http://www.jhipster.tech/documentation-archive/v4.8.2/production/
[Running tests page]: http://www.jhipster.tech/documentation-archive/v4.8.2/running-tests/
[Setting up Continuous Integration]: http://www.jhipster.tech/documentation-archive/v4.8.2/setting-up-ci/


[Node.js]: https://nodejs.org/
[Yarn]: https://yarnpkg.org/
[Webpack]: https://webpack.github.io/
[Angular CLI]: https://cli.angular.io/
[BrowserSync]: http://www.browsersync.io/
[Karma]: http://karma-runner.github.io/
[Jasmine]: http://jasmine.github.io/2.0/introduction.html
[Protractor]: https://angular.github.io/protractor/
[Leaflet]: http://leafletjs.com/
[DefinitelyTyped]: http://definitelytyped.org/
