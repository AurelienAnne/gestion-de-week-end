package glitaa.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
public class City implements Serializable, Location {
	
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String departmentCode;
	//private Department department;
	private String zipCode;
	private String cityName;
	private Integer population2012;
	private Double longitude;
	private Double latitude;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }
	public void setId(Long id) {
		this.id = id;
	}		
	
	public String getDepartmentCode() {
		return departmentCode;
	}
	public void setDepartmentCode(String departmentCode) {
		this.departmentCode = departmentCode;
	}
	
	/*@ManyToOne
	public Department getDepartment() {
		return department;
	}
	public void setDepartment(Department department) {
		this.department = department;
	}*/
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public Integer getPopulation2012() {
		return population2012;
	}
	public void setPopulation2012(Integer population2012) {
		this.population2012 = population2012;
	}
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        City city = (City) o;
        if (city.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), city.getId());
    }
	
	@Override
	public String toString() {
		return getClass().getSimpleName() + " : " + getId() + " " + getCityName() + " " + getZipCode()
			+ " " + getPopulation2012() + " " + getLongitude() + " " + getLatitude();
	}
}
