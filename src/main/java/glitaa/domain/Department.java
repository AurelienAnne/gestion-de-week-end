package glitaa.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

@Entity
public class Department implements Serializable, Location  {

	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String departmentCode;
	private String departmentName;
	private Set<City> cities = new HashSet<>();
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public String getDepartmentCode() {
		return departmentCode;
	}
	public void setDepartmentCode(String departmentCode) {
		this.departmentCode = departmentCode;
	}
	
	public String getDepartmentName() {
		return departmentName;
	}
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}
	
	// TODO : Remplacer par une relation avec id et non la colonne departmentCode (Cause de l'erreur LazyInitializationException ?)
	@OneToMany(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
	@JoinColumn(name = "departmentCode", referencedColumnName = "departmentCode")
	public Set<City> getCities() {
		return cities;
	}
	public void setCities(Set<City> cities) {
		this.cities = cities;
	}		
	
	@Override
	public String toString() {
		return getClass().getSimpleName() + " : " + getId() + " " + getDepartmentName() + " " + getCities();
	}
}
