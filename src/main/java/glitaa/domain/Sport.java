package glitaa.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A Sport.
 */
@Entity
@Table(name = "sport")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Sport implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String sportName;
    
    private Boolean windRequired;
    
    private Boolean snowRequired;

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSportName() {
        return sportName;
    }
    
    public Sport sportName(String sportName) {
        this.sportName = sportName;
        return this;
    }

    public void setSportName(String sportName) {
        this.sportName = sportName;
    }

	public Boolean isWindRequired() {
		return windRequired;
	}

	public void setWindRequired(Boolean windRequired) {
		this.windRequired = windRequired;
	}

	public Boolean isSnowRequired() {
		return snowRequired;
	}

	public void setSnowRequired(Boolean snowRequired) {
		this.snowRequired = snowRequired;
	}
    // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove
	
	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Sport sport = (Sport) o;
        if (sport.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), sport.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Sport{" +
            "id=" + getId() +
            ", sportname='" + getSportName() + "'" +
            "}";
    }
}

    