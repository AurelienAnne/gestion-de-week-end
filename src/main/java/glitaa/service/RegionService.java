package glitaa.service;

import glitaa.domain.Department;
import glitaa.domain.Region;
import glitaa.repository.RegionRepository;
import glitaa.service.dto.RegionDTO;
import java.util.Set;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class RegionService {
    private final RegionRepository regionRepository;

    public RegionService(RegionRepository regionRepository) {
        this.regionRepository = regionRepository;
    }

    @Transactional(readOnly = true)
    public Page<RegionDTO> getAll(Pageable pageable) {
        return regionRepository.findAll(pageable).map(RegionDTO::new);
    }

	public Set<Department> getDepartmentsOf(Long id) {
		return regionRepository.getOne(id).getDepartments();     
	}
	
	public Region getOne(Long id) {
		return regionRepository.getOne(id);     
	}
}
