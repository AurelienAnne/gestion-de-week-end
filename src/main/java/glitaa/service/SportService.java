package glitaa.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import glitaa.domain.Sport;
import glitaa.repository.SportRepository;

@Service
@Transactional
public class SportService {

	private final SportRepository sportRepository;

	public SportService(SportRepository sportRepository) {
		this.sportRepository = sportRepository;
	}
	
	@Transactional(readOnly = true)
    public List<Sport> getAll() {
        return sportRepository.findAll();
    }
}
