package glitaa.service;

import glitaa.domain.City;
import glitaa.domain.Sport;
import glitaa.domain.User;
import glitaa.service.dto.CityDTO;
import glitaa.service.dto.DailyWeatherDTO;
import glitaa.service.dto.DayDTO;
import glitaa.service.dto.LocationDatedNotesDTO;
import glitaa.service.dto.NoteSportDTO;
import glitaa.service.dto.OpenWeatherMapResponseDTO;
import glitaa.service.dto.SportDTO;
import glitaa.service.dto.WeatherDTO;
import glitaa.service.dto.openweathermap.ListElement;
import glitaa.service.dto.openweathermap.Weather;
import glitaa.service.dto.openweathermap.Wind;
import glitaa.service.util.ServiceCache;
import glitaa.service.util.ServiceException;
import glitaa.web.rest.util.HeaderUtil;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;
import org.springframework.beans.factory.annotation.Value;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class WeatherService {

	private final Logger log = LoggerFactory.getLogger(WeatherService.class);
	private static final String ENTITY_NAME = "weatherService";

	private final SportService sportService;
	private final AccountService accountService;
	private int nbRequetesOWM;
	private Long lastOwmCallTimestamp;

	@Value("${owm_apikey}")
	private String ApiKey;
	
	@Value("${owm_maxRequestPerMin}")
	private Integer maxRequestPerMin;

	private String UrlApi() {
		return "http://api.openweathermap.org/data/2.5/forecast?appid=" + this.ApiKey + "&units=metric&lang=fr";
	}

	public WeatherService(AccountService accountService, SportService sportService) {
		super();
		this.accountService = accountService;
		this.sportService = sportService;
		this.nbRequetesOWM = 0;
		this.lastOwmCallTimestamp = (long)0;
	}
	
	private Long getCurrentTimestamp() {
		return System.currentTimeMillis() / 1000;
	}
	
	public List<WeatherDTO> getWeatherByZipCode(String zipCode, Boolean useCache) throws ServiceException {	
		// Vérifications
		if(!zipCode.matches("[0-9]{5}")) {
			throw new ServiceException(400, "zipCodeFormatError", "Zip code format invalid : " + zipCode );
		}
		
		if(useCache) {
			List<WeatherDTO> cache = ServiceCache.getInstance().get("Weather/getWeatherByZipCode/" + zipCode + ".json", new TypeReference<List<WeatherDTO>>(){});
			if(cache != null) { // S'il existe un cache
				return cache;
			}
		}

		// OpenWeatherMap limite à 60 requetes par minute, 
		// donc on attend le nombre de seconde restante au bout de 60 requetes pour ne pas bloquer l'API		
		Long now = this.getCurrentTimestamp();
		if(this.lastOwmCallTimestamp == 0) { // Premiere recherche
			this.lastOwmCallTimestamp = this.getCurrentTimestamp();
		}
		if(now <= this.lastOwmCallTimestamp + 60) {
			if(this.nbRequetesOWM >= maxRequestPerMin) {
				try {
					log.info("WeatherService.getWeatherByZipCode() : OWM calls limit reached (" + this.nbRequetesOWM  + " req/min). Paused for " + ((this.lastOwmCallTimestamp + 60) - now) + " sec");
					Thread.sleep(((this.lastOwmCallTimestamp + 60) - now) * 1000);
				} catch (InterruptedException e1) {
					log.warn(e1.getMessage());
				}
				this.nbRequetesOWM = 0;
			}
			this.nbRequetesOWM++;			
		} else {
			this.lastOwmCallTimestamp = now;
			this.nbRequetesOWM = 0;
		}
		
		// Appel à l'API de OpenWeatherMap
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> response = null;
		try {
			response = restTemplate.getForEntity(this.UrlApi() + "&zip=" + zipCode + ",fr", String.class);
		} catch(HttpClientErrorException ex) {
			if(ex.getStatusCode() == HttpStatus.INTERNAL_SERVER_ERROR) {
				throw new ServiceException(500, "owmError", "OpenWeatherMap error 500 for this zip code : " + zipCode + "\n" + "Exception : " + ex.getMessage());
			}
			else if(ex.getStatusCode() == HttpStatus.NOT_FOUND) {
				throw new ServiceException(404, "cityNotExists", "No city for this zip code : " + zipCode);
			}
			else {
				throw new ServiceException(ex.getRawStatusCode(), "owmUnknowError", ex.getMessage());
			}
		} catch(ResourceAccessException ex) {
			throw new ServiceException(500, "owmNetworkError", ex.getMessage());
		}

		// Conversion du JSON de OpenWeatherMap en objet OpenWeatherMapResponseDTO
		ObjectMapper mapper = new ObjectMapper();
		OpenWeatherMapResponseDTO OWMResponse = null;
		try {
			OWMResponse = mapper.readValue(response.getBody(), OpenWeatherMapResponseDTO.class);
		} catch (Exception e1) {
			log.error(e1.getMessage());
		}

		// Création de la liste retournée au client
		ArrayList<WeatherDTO> listWeatherDTO = new ArrayList<>();
		if(OWMResponse != null) {
			for(ListElement weatherReport : OWMResponse.getList()) {
				Calendar calendar = Calendar.getInstance();
				Date weatherDate = new Date((long)weatherReport.getDt()*1000);
				calendar.setTime(weatherDate);
				int day = calendar.get(Calendar.DAY_OF_WEEK); 
				// 7 = Saturday - 1 = Sunday // On est intéressé que par le week-end
				if(day == 7 || day == 1) {
					listWeatherDTO.add(new WeatherDTO(weatherReport));
				}

			}
		}

		ServiceCache.getInstance().set("Weather/getWeatherByZipCode/" + zipCode + ".json", listWeatherDTO);

		return listWeatherDTO;
	}

	public List<DailyWeatherDTO> getDailyWeatherByZipCode(String zipCode) throws ServiceException {
		// Vérifications
		if(!zipCode.matches("[0-9]{5}")) {
			throw new ServiceException(400, "zipCodeFormatError", "Zip code format invalid : " + zipCode );
		}

		/*List<DailyWeatherDTO> cache = ServiceCache.getInstance().get("Weather/getDailyWeatherByZipCode/" + zipCode + ".json", new TypeReference<List<DailyWeatherDTO>>(){}, 7200);
		if(cache != null) { // S'il existe un cache de moins de 120 mins
			return cache;
		}*/

		List<DailyWeatherDTO> listDailyWeatherDTO = new ArrayList<>();

		List<WeatherDTO> listWeatherDTO = this.getWeatherByZipCode(zipCode, true);

		/* Variables pour calculer les moyennes */
		Double sumTemperature = 0.0;
		Double sumPressure = 0.0;
		Double sumSea_level = 0.0;
		Double sumGrnd_level = 0.0;
		Double sumHumidity = 0.0;
		Double sumRain = 0.0;
		Double sumSnow = 0.0;
		Integer sumClouds = 0;
		Double sumWindSpeed = 0.0;
		Double sumWindDegree = 0.0;
		int nb = 0;

		for (WeatherDTO weatherDTO : listWeatherDTO) {
			if(listDailyWeatherDTO.isEmpty()) {
				listDailyWeatherDTO.add(new DailyWeatherDTO(weatherDTO));

				/* Initialiser les variables */
				sumTemperature = weatherDTO.getTemperature();
				sumPressure = weatherDTO.getPressure();
				sumSea_level = weatherDTO.getSea_level();
				sumGrnd_level = weatherDTO.getGrnd_level();
				sumHumidity = weatherDTO.getHumidity();
				if(weatherDTO.getRain() != null) {
					sumRain = weatherDTO.getRain();
				}
				if(weatherDTO.getSnow() != null) {
					sumSnow = weatherDTO.getSnow();
				}
				sumClouds = weatherDTO.getClouds();
				sumWindSpeed = weatherDTO.getWind().getSpeed();
				sumWindDegree = weatherDTO.getWind().getDeg();
				nb = 1;
			}else {
				Date weatherDate = new Date ((long)weatherDTO.getTimestamp()*1000);
				DailyWeatherDTO lastDailyWeather = listDailyWeatherDTO.get(listDailyWeatherDTO.size()-1);
				Date lastWeatherDate = new Date ((long)lastDailyWeather.getTimestamp()*1000);

				if(weatherDate.getDate() != lastWeatherDate.getDate()) {
					if(nb > 0) {
						/* Mettre les moyennes dans le DailyWeather precedent */
						lastDailyWeather.setTemperature(sumTemperature/nb);
						lastDailyWeather.setPressure(sumPressure/nb);
						lastDailyWeather.setSea_level(sumSea_level/nb);
						lastDailyWeather.setGrnd_level(sumGrnd_level/nb);
						lastDailyWeather.setHumidity(sumHumidity/nb);
						lastDailyWeather.setClouds(sumClouds/nb);
						lastDailyWeather.setRain(sumRain);
						lastDailyWeather.setSnow(sumSnow);
						Wind newWind = new Wind();
						newWind.setSpeed(sumWindSpeed/nb);
						newWind.setDeg(sumWindDegree/nb);
						lastDailyWeather.setWind(newWind);
					}

					/* Initialiser les variables */
					listDailyWeatherDTO.add(new DailyWeatherDTO(weatherDTO));
					sumTemperature = weatherDTO.getTemperature();
					sumPressure = weatherDTO.getPressure();
					sumSea_level = weatherDTO.getSea_level();
					sumGrnd_level = weatherDTO.getGrnd_level();
					sumHumidity = weatherDTO.getHumidity();
					if(weatherDTO.getRain() != null) {
						sumRain = weatherDTO.getRain();
					}
					if(weatherDTO.getSnow() != null) {
						sumSnow = weatherDTO.getSnow();
					}
					sumClouds = weatherDTO.getClouds();
					sumWindSpeed = weatherDTO.getWind().getSpeed();
					sumWindDegree = weatherDTO.getWind().getDeg();
					nb = 1;
				}else {
					sumTemperature += weatherDTO.getTemperature();
					sumPressure += weatherDTO.getPressure();
					sumSea_level += weatherDTO.getSea_level();
					sumGrnd_level += weatherDTO.getGrnd_level();
					sumHumidity += weatherDTO.getHumidity();
					sumClouds += weatherDTO.getClouds();
					
					// Rain peut etre null
					if(weatherDTO.getRain() != null) {
						sumRain += weatherDTO.getRain();
					}

					// Snow peut etre null
					if(weatherDTO.getSnow() != null) {
						sumSnow += weatherDTO.getSnow();
					}

					for (Weather weather : weatherDTO.getWeathers()) {
						lastDailyWeather.addWeather(weather);
					}

					sumWindSpeed += weatherDTO.getWind().getSpeed();
					sumWindDegree += weatherDTO.getWind().getDeg();

					nb++;
				}
			}

		}

		// Enregistre les dernieres valeurs
		DailyWeatherDTO lastDailyWeather = listDailyWeatherDTO.get(listDailyWeatherDTO.size()-1);
		if(nb > 0) {
			/* Set the mean in the previous daily weather */
			lastDailyWeather.setTemperature(sumTemperature/nb);
			lastDailyWeather.setPressure(sumPressure/nb);
			lastDailyWeather.setSea_level(sumSea_level/nb);
			lastDailyWeather.setGrnd_level(sumGrnd_level/nb);
			lastDailyWeather.setHumidity(sumHumidity/nb);
			lastDailyWeather.setClouds(sumClouds/nb);
			lastDailyWeather.setRain(sumRain);
			lastDailyWeather.setSnow(sumSnow);
			Wind newWind = new Wind();
			newWind.setSpeed(sumWindSpeed/nb);
			newWind.setDeg(sumWindDegree/nb);
			lastDailyWeather.setWind(newWind);
		}
		
		//ServiceCache.getInstance().set("Weather/getDailyWeatherByZipCode/" + zipCode + ".json", listDailyWeatherDTO);

		return listDailyWeatherDTO;
	}

	public List<DayDTO> getWeatherEvaluationByZipCode(String zipCode) throws ServiceException {
		// Vérifications
		if(!zipCode.matches("[0-9]{5}")) {
			throw new ServiceException(400, "zipCodeFormatError", "Zip code format invalid : " + zipCode );
		}

		/*List<DayDTO> cache = ServiceCache.getInstance().get("Weather/getWeatherEvaluationByZipCode/" + zipCode + ".json", new TypeReference<List<DayDTO>>(){}, 7200);
		if(cache != null) { // S'il existe un cache de moins de 120 mins
			return cache;
		}*/

		ArrayList<DayDTO> listNotes = new ArrayList<>();

		List<DailyWeatherDTO> weatherList = this.getDailyWeatherByZipCode(zipCode);

		ArrayList<Double> listNotesTemp = new ArrayList<>();

		for (DailyWeatherDTO weatherDTO : weatherList) {
			for(Weather weather : weatherDTO.getWeathers()) {
				int weatherId = weather.getId();
				Double result = getNoteByWeatherId(weatherId);
				if(result.equals(0.0)) { // Extreme weather
					break;
				} else if (result.equals(-1.0)) { // Bad weather id
					throw new ServiceException(520, "BadWeatherID", "OpenWeatherMap error : Bad weather id");
				} else {
					listNotesTemp.add(getNoteByWeatherId(weatherId));
				}
			}
			
			double sum = 0;
			for (Double d : listNotesTemp) {
				sum += d;
			}
			double note = sum / listNotesTemp.size();
			listNotesTemp.clear();
			
			// Evalue la note avec des malus
			Double noteGlobale = note - getTemperatureMalus(weatherDTO.getTemperature());
			noteGlobale-= getWindMalus(weatherDTO.getWind().getSpeed());

			/* Evaluation des notes de sport */
			List<NoteSportDTO> notesSport = getNotesSport(note, weatherDTO);
			
			for (NoteSportDTO noteSportDTO : notesSport) {
				noteGlobale += noteSportDTO.getNote();
			}
			noteGlobale = noteGlobale / (notesSport.size()+1);

			/* Creation des DayDTO */
			List<WeatherDTO> weathers = this.getWeatherByZipCode(zipCode, true);
			Calendar calendar = Calendar.getInstance();
			Date dailyWeatherDate = new Date((long)weatherDTO.getTimestamp()*1000);
			calendar.setTime(dailyWeatherDate);
			int day = calendar.get(Calendar.DAY_OF_WEEK);

			for (int i=0; i < weathers.size(); i++) {
				WeatherDTO weatherDTO2 = weathers.get(i);
				Date weatherDate = new Date((long)weatherDTO2.getTimestamp()*1000);
				calendar.setTime(weatherDate);
				int weatherDay = calendar.get(Calendar.DAY_OF_WEEK);
				// 7 = Saturday - 1 = Sunday // Si le jour est différent, en enleve l'element
				if(weatherDay != day) {
					weathers.remove(i);
					i--;
				}
			}

			if(day == 7) {
				listNotes.add(new DayDTO(note, noteGlobale, notesSport, weatherDTO, weathers, true));
			}
			else {
				listNotes.add(new DayDTO(note, noteGlobale, notesSport, weatherDTO, weathers, false));
			}
		}

		//ServiceCache.getInstance().set("Weather/getWeatherEvaluationByZipCode/" + zipCode + ".json", listNotes);

		return listNotes;
	}

	public List<LocationDatedNotesDTO> getAllWeatherEvaluations(User user) throws ServiceException {
		ArrayList<LocationDatedNotesDTO> weatherNotes = new ArrayList<>();

		for (City city : accountService.getPreferencesAllCities(user)) {
			try {
				List<DayDTO> satAndSunWeathers = getWeatherEvaluationByZipCode(city.getZipCode());
				if(satAndSunWeathers.size() < 2) {
					throw new ServiceException(500, "weatherNotAvailable", "Weather not available");
				}

				DayDTO first = satAndSunWeathers.get(0);
				Double moyenne = (satAndSunWeathers.get(0).getNoteFinale() + satAndSunWeathers.get(1).getNoteFinale())/2;

				if(first.isSaturday()) {
					weatherNotes.add(new LocationDatedNotesDTO(new CityDTO(city), moyenne, satAndSunWeathers.get(0), satAndSunWeathers.get(1)));
				}
				else {
					weatherNotes.add(new LocationDatedNotesDTO(new CityDTO(city), moyenne, satAndSunWeathers.get(1), satAndSunWeathers.get(0)));
				}

			} catch (ServiceException e) {
				throw new ServiceException(520, "Erreur WeatherService : ", "Cannot getWeatherEvaluationByZipCode("+ city.getZipCode() +")");
			}

			nbRequetesOWM++;
		}

		return weatherNotes;
	}

	public List<LocationDatedNotesDTO> getBestNotes(int nbOfNotesToReturn, User user) throws ServiceException {
		List<LocationDatedNotesDTO> allNotes = getAllWeatherEvaluations(user);

		allNotes.sort(new NotesComparator());
		
		Set<Sport> userSports = user.getPreferencesSports();
		
		// Transformer les sports de l'utilisateur en SportDTO pour la comparaison
		List<SportDTO> userSportsDTO = new ArrayList<>();
		for (Sport sport : userSports) {
			userSportsDTO.add(new SportDTO(sport));
		}

		// Si la liste des notes est plus petite que le nombre de résultats à afficher, 
		// on retourne la liste complète
		if(allNotes.size() < nbOfNotesToReturn) {
			removeUselessSports(allNotes, userSportsDTO);
			return allNotes;
		}

		List<LocationDatedNotesDTO> bestNotes = new ArrayList<>();

		for (int i = 0; i < nbOfNotesToReturn; i++) {
			bestNotes.add(allNotes.get(i));
		}

		removeUselessSports(bestNotes, userSportsDTO);
		
		return bestNotes;

	}

	/*					*
	 * Classes Internes *
	 *                  */

	/**
	 * Trie une liste de LocationDatedNotesDTO de façon décroissante
	 * @author aurelienanne
	 *
	 */
	class NotesComparator implements Comparator<LocationDatedNotesDTO>{

		@Override
		public int compare(LocationDatedNotesDTO o1, LocationDatedNotesDTO o2) {
			if(o1 == null && o2 == null) return 0;
			else if(o1 == null) return 1;
			else if(o2 == null) return -1;

			if(o1.getNote() < o2.getNote()) return 1;
			else if(o1.getNote() > o2.getNote()) return -1;
			else return 0;
		}

	}
	
	/**
	 * Trie une liste de NoteSportDTO de façon décroissante
	 * @author aurelienanne
	 *
	 */
	class NotesSportComparator implements Comparator<NoteSportDTO>{

		@Override
		public int compare(NoteSportDTO o1, NoteSportDTO o2) {
			if(o1 == null && o2 == null) return 0;
			else if(o1 == null) return 1;
			else if(o2 == null) return -1;
			
			if(o1.getNote() < o2.getNote()) return 1;
			else if(o1.getNote() > o2.getNote()) return -1;
			else return 0;
		}

	}

	/*					*
	 * Méthodes Locales *
	 *                  */	

	/**
	 * Supprime, pour l'affichage, les notes des sports que l'utilisateur ne pratique pas
	 * @param notes
	 * @param userSportsDTO
	 */
	private void removeUselessSports(List<LocationDatedNotesDTO> notes, List<SportDTO> userSportsDTO) {
		boolean trouve = false;
		
		for (LocationDatedNotesDTO note : notes) {
			List<NoteSportDTO> notesSportSat = note.getSat().getNotesSport();
			
			for (int i=0 ; i < notesSportSat.size(); i++) {
				NoteSportDTO noteSport = notesSportSat.get(i);
				trouve = false;
				
				for (SportDTO sportDTO : userSportsDTO) {
					if(sportDTO.getId() == noteSport.getSport().getId()) {
						trouve = true;
					}
				}
				
				if(!trouve) {
					notesSportSat.remove(i);
					i--;
				}
			}
			
			List<NoteSportDTO> notesSportSun = note.getSun().getNotesSport();
			
			for(int i=0; i < notesSportSun.size(); i++) {
				NoteSportDTO noteSport = notesSportSun.get(i);
				trouve = false;
				
				for (SportDTO sportDTO : userSportsDTO) {
					if(sportDTO.getId() == noteSport.getSport().getId()) {
						trouve = true;
					}
				}
				
				if(!trouve) {
					notesSportSun.remove(i);
					i--;
				}
			}
		}
	}
	
	/**
	 * Calcule les notes de tous les sports pour une journée et une ville données
	 * @param note
	 * @param weatherDTO
	 * @return
	 */
	private List<NoteSportDTO> getNotesSport(Double note, DailyWeatherDTO weatherDTO) {
		List<NoteSportDTO> listNotesSport = new ArrayList<>();

		List<Sport> sports = sportService.getAll();

		for (Sport sport : sports) {
			
			Double newNote = note;
			// Enlever les sports qui ont besoin de conditions particulières
			boolean snowRequired = sport.isSnowRequired(); // true -> ski // false -> tous les autres sports

			// Si pas besoin de neige et que neige > 40 mm
			if(!snowRequired && weatherDTO.getSnow() != null) {
				if(weatherDTO.getSnow() > 40) {
					listNotesSport.add(new NoteSportDTO(new SportDTO(sport), 0.0));
					continue;
				}
			}

			// Si besoin de neige mais pas de neige
			if (snowRequired && weatherDTO.getSnow() == null) {
				listNotesSport.add(new NoteSportDTO(new SportDTO(sport), 0.0));
				continue;
			}else if(snowRequired && weatherDTO.getSnow() != null) {
				if(weatherDTO.getSnow() <= 0) {
					listNotesSport.add(new NoteSportDTO(new SportDTO(sport), 0.0));
					continue;
				}
			}

			//Bonus / malus selon le temps
			boolean windRequired = sport.isWindRequired(); // true -> bonus // false -> malus

			// Supposons que le vent minimal est de 2.5m/s et le vent maximal pour la voile est de 10m/s
			if(windRequired) {
				
				Double windSpeed = weatherDTO.getWind().getSpeed();
				if(windSpeed >= 2.5 && windSpeed <= 10.0) {
					Double bonus = windSpeed / 10.0; // Bonus entre 0.35 et 1
					newNote+= bonus;
					
					if(newNote > 5.0) newNote = 5.0;
					listNotesSport.add(new NoteSportDTO(new SportDTO(sport), newNote));
					continue;
				}
				else { // Pas assez de vent OU trop de vent = note à 0.0
					listNotesSport.add(new NoteSportDTO(new SportDTO(sport), 0.0));
					continue;
				}
			}else {
				Double windSpeed = weatherDTO.getWind().getSpeed();
				Double malus = getWindMalus(windSpeed);
				
				newNote -= malus;
				listNotesSport.add(new NoteSportDTO(new SportDTO(sport), newNote));
			}
		}

		listNotesSport.sort(new NotesSportComparator());
		
		return listNotesSport;
	}

	/* Ideal temperature = 25°C */
	private double getTemperatureMalus(Double temperature) {
		if(temperature > 16 && temperature < 27 )
			return 0.0;
		else if(temperature > 9 && temperature < 30)
			return 0.5;
		else if(temperature > 4 && temperature < 36)
			return 1.0;
		else 
			return 2.0;
	}

	/* A partir de 7m/s de vent, malus || 13m/s = malus de -1.0 */
	private Double getWindMalus(Double speed) {
		Double malus = (speed - 7.0) / 6.0; 
		if(malus<0) malus = 0.0;
		return malus;
	}

	private Double getNoteByWeatherId(int weatherId) {
		switch(weatherId) {
		/* Perfect weather = 5 */
		case 800: // clear sky
		case 951: // calm
			return 5.0;

			/* Good weather = 4 */
		case 801: // few clouds 
		case 802: // scattered clouds 
		case 803: // broken clouds 
		case 952: // light breeze 
		case 953: // gentle breeze 
			return 4.0;

			/* Intermediate weather = 3 */
		case 300: // light intensity drizzle 
		case 310: // light intensity drizzle rain 
		case 500: // light rain 
		case 501: // moderate rain 
		case 600: // light snow
		case 620: // light shower snow 
		case 701: // mist
		case 711: // smoke
		case 721: // haze
		case 731: // sand, dust whirls 
		case 741: // fog
		case 751: // sand
		case 804: // overcast clouds 
		case 954: // moderate breeze 
		case 955: // fresh breeze 
		case 956: // strong breeze 
		case 957: // high wind, near gale 
		case 958: // gale
			return 3.0;

			/* Bad weather = 2 */
		case 200: // thunderstorm with light rain 
		case 201: // thunderstorm with rain 
		case 210: // light thunderstorm 
		case 211: // thunderstorm 
		case 230: // thunderstorm with light drizzle 
		case 231: // thunderstorm with drizzle 
		case 301: // drizzle
		case 302: // heavy intensity drizzle 
		case 311: // drizzle rain 
		case 312: // heavy intensity drizzle rain 
		case 313: // shower rain and drizzle 
		case 321: // shower drizzle 
		case 502: // heavy intensity rain 
		case 520: // light intensity shower rain
		case 521: // shower rain
		case 601: // snow
		case 611: // sleet
		case 612: // shower sleet 
		case 615: // light rain and snow
		case 616: // rain and snow 
		case 621: // shower snow 
		case 761: // dust
		case 762: // volcanic ash
		case 771: // squalls
		case 959: // severe gale
			return 2.0;

			/* Awful weather = 1 */
		case 202: // thunderstorm with heavy rain
		case 212: // heavy thunderstorm 
		case 221: // ragged thunderstorm 
		case 232: // thunderstorm with heavy drizzle 
		case 314: // heavy shower rain and drizzle 
		case 503: // very heavy rain
		case 504: // extreme rain
		case 511: // freezing rain
		case 522: // heavy intensity shower rain 
		case 531: // ragged shower rain
		case 602: // heavy snow 
		case 622: // heavy shower snow
		case 781: // tornado
		case 960: // storm
		case 961: // violent storm
			return 1.0;

			/* Extreme weather = 0 */
		case 900: // tornado
		case 901: // tropical storm 
		case 902: // hurricane
		case 903: // cold
		case 904: // hot
		case 905: // windy
		case 906: // hail
		case 962: // hurricane
			return 0.0;

			/* Weather id not in the API list = Error -1*/
		default:
			log.error("Weather id not recognized : " + weatherId);
			return -1.0;
		}
	}
}
