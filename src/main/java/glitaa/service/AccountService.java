package glitaa.service;

import glitaa.domain.Location;
import glitaa.domain.City;
import glitaa.domain.Department;
import glitaa.domain.Region;
import glitaa.domain.User;
import glitaa.repository.AuthorityRepository;
import glitaa.repository.PersistentTokenRepository;
import glitaa.repository.UserRepository;
import glitaa.service.dto.AccountPreferencesDTO;
import glitaa.service.dto.LocationCityDTO;
import glitaa.service.dto.LocationDepartmentDTO;
import glitaa.service.dto.LocationElementDTO;
import glitaa.service.dto.LocationsDTO;
import glitaa.service.util.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.*;

@Service
@Transactional
public class AccountService {

    private final Logger log = LoggerFactory.getLogger(AccountService.class);

    private final PersistentTokenRepository persistentTokenRepository;

    private final AuthorityRepository authorityRepository;
    
    private final UserRepository userRepository;
    
    private final CacheManager cacheManager;    
    
    private final DepartmentService departmentService;
    
    private final RegionService regionService;
    
    private final CityService cityService;
    
    private final UserService userService;
    
    @Value("${minPopulationRegion}")
    private Integer minPopulationRegionSetting;
    
    private Integer minPopulationRegion;

    public AccountService(PersistentTokenRepository persistentTokenRepository, AuthorityRepository authorityRepository, CacheManager cacheManager, DepartmentService departmentService, RegionService regionService, CityService cityService, UserService userService, UserRepository userRepository) {
        this.persistentTokenRepository = persistentTokenRepository;
        this.authorityRepository = authorityRepository;
        this.cacheManager = cacheManager;
        this.regionService = regionService;
        this.departmentService = departmentService;
        this.cityService = cityService;
        this.userService = userService;
        this.userRepository = userRepository;
    }
        
    
    /** Préférences **/
    
    /**
     * Modifie cities1 en ajoutant la liste des villes contenues dans cities2 dont la population a une valeur >=  minPopulationRegion
     * @param locationCitiesDTO Liste des villes reçues (id) lors de l'enregistrement des préférences
     */
    private void addCities(Set<City> cities1, Set<City> cities2) {   	
    	for(City city2 : cities2) {
    		if(!this.elementExists((Set<City>) cities1, city2.getId()) && city2.getPopulation2012() >= this.minPopulationRegion) {
    			cities1.add(city2);
    		}
    	}
    }
    
    /**
     * @param locationCitiesDTO Liste des villes reçues (id) lors de l'enregistrement des préférences
     * @return Retourne la liste des villes sans doublons
     */
    private Set<City> getCities(ArrayList<LocationCityDTO> locationCitiesDTO) {
    	Set<City> cities = new HashSet<>();
    	
    	for(LocationElementDTO city : locationCitiesDTO) {
    		if(!this.elementExists(cities, city.getId())) {
    			cities.add(cityService.findOne(city.getId()));
    		}
    	}    	
    	return cities;
    }
    

    /**
     * @param locationCitiesDTO Liste des departements reçus (id) lors de l'enregistrement des préférences
     * @return Retourne la liste des departements sans doublons
     */
	private Set<Department> getDepartments(ArrayList<LocationDepartmentDTO> locationDepartmentDTO) {
		Set<Department> departments = new HashSet<>();
		
		for(LocationDepartmentDTO department : locationDepartmentDTO) {
			if(!this.elementExists(departments, department.getId())) {
				departments.add(departmentService.getOne(department.getId()));
    		}
    	} 
		return departments;
	}

	/**
     * @param locationCitiesDTO Liste des regions reçues (id) lors de l'enregistrement des préférences
     * @return Retourne la liste des regions sans doublons
     */
	private Set<Region> getRegions(ArrayList<LocationElementDTO> locationElementDTO) {
		Set<Region> regions = new HashSet<>();
		
		for(LocationElementDTO region : locationElementDTO) {
			if(!this.elementExists(regions, region.getId())) {
				regions.add(regionService.getOne(region.getId()));
    		}
    	}
		return regions;
	}
    
	/**
	 * @param currentlist Liste à vérifier
	 * @param id Identifiant de l'élément à rechercher
	 * @return Booléan indiquant si l'élément d'identifiant 'id' existe dans la liste 'list'
	 */
    private <T> boolean elementExists(Set<T> list, Long id) {
    	if(list.size() == 0) {
			return false;
		}

    	for(T location : list) {
    		if(Objects.equals(((Location) location).getId(), id)) {
    			return true;
    		}
    	}
    	
		return false;
    }


    @Transactional(readOnly=true)
	public AccountPreferencesDTO getPreferences() {
		User user = userService.getUserWithAuthorities();
    	
 		AccountPreferencesDTO preferences = new AccountPreferencesDTO();
 		preferences.setSports(user.getPreferencesSports());
 		
 		LocationsDTO locations = new LocationsDTO();
 		user.getPreferencesCities().forEach((city)-> locations.addCity(city));
 		user.getPreferencesDepartments().forEach((department)-> locations.addDepartment(department));
 		user.getPreferencesRegions().forEach((region)-> locations.addRegion(region)); 	
 		
 		preferences.setLocations(locations);

 		return preferences;  
	}
	
	public void setPreference(AccountPreferencesDTO userPreferences) throws ServiceException {
		User user = userService.getUserWithAuthorities();

    	try {
	    	user.setPreferencesSports(userPreferences.getSports());	    	
	    	user.setPreferencesCities(this.getCities(userPreferences.getLocations().getCities()));
	    	user.setPreferencesDepartments(this.getDepartments(userPreferences.getLocations().getDepartments()));
	    	user.setPreferencesRegions(this.getRegions(userPreferences.getLocations().getRegions()));
	    	userRepository.save(user);
    	}
    	catch (Exception e) {
    		throw new ServiceException(400, "savePreferenceError", e.getMessage());
    	}
	}
	
	/**
	 * Retourne les villes et les villes présentent dans les départements et régions des préférences de l'utilisateur
	 */
	public Set<City> getPreferencesAllCities() {
		Set<City> cities = new HashSet<>();
		User user = userService.getUserWithAuthorities();
		
		this.minPopulationRegion = (int) Math.round(this.minPopulationRegionSetting * (user.getPreferencesRegions().size() + (user.getPreferencesDepartments().size() * 0.5))); 
		
		user.getPreferencesCities().forEach((city)-> cities.add(city));
		
		user.getPreferencesDepartments().forEach((department)-> this.addCities(cities, department.getCities()));
		
		user.getPreferencesRegions().forEach((region)-> {
			region.getDepartments().forEach((department) -> {
				this.addCities(cities, department.getCities());
			});
		});
		
		return cities;
	}


	public Set<City> getPreferencesAllCities(User user) {
		Set<City> cities = new HashSet<>();
		
		this.minPopulationRegion = (int) Math.round(this.minPopulationRegionSetting * (user.getPreferencesRegions().size() + (user.getPreferencesDepartments().size() * 0.5))); 
		
		user.getPreferencesCities().forEach((city)-> cities.add(city));
		
		user.getPreferencesDepartments().forEach((department)-> this.addCities(cities, department.getCities()));
		
		user.getPreferencesRegions().forEach((region)-> {
			region.getDepartments().forEach((department) -> {
				this.addCities(cities, department.getCities());
			});
		});
		
		return cities;
	}
}
