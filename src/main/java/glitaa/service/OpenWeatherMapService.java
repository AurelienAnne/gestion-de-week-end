package glitaa.service;

import glitaa.domain.User;
import glitaa.service.util.ServiceException;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
@EnableScheduling
public class OpenWeatherMapService {

	private final Logger log = LoggerFactory.getLogger(OpenWeatherMapService.class);
	private static final String ENTITY_NAME = "openWeatherMapService";

	private final CityService cityService;
	private final WeatherService weatherService;
	private final UserService userService;
	private final MailService mailService;
	
	public OpenWeatherMapService(CityService cityService, WeatherService weatherService,
			UserService userService, MailService mailService) {
		super();
		this.cityService = cityService;
		this.weatherService = weatherService;
		this.userService = userService;
		this.mailService = mailService;
	}
	
	private Integer lastPercentProgress = -1;
	private void logProgress(Integer current, Integer total, String description) {
		Integer currentPercentProgress = Math.round(((float)current / (float)total)*100);
		if(currentPercentProgress != this.lastPercentProgress) {
			log.info("[Cron] " + description + " : " + current + "/" + total + "(" + currentPercentProgress + "%)");
			this.lastPercentProgress = currentPercentProgress;
		}		
	}
	
	@Scheduled(cron = "0 0 1 * * 3-5") // Du mercredi au vendredi à 01h00m00s. La tâche met environ 30 sec à se lancer
	public void loadAll() {		
		List<String> zipCodes = cityService.getAllZipcode();
		Integer count = 0;
		ArrayList<String> unknowZipCode = new ArrayList<>();
		
		log.info("[Cron] Load all OWM zip codes started");
		for(count = 0; count < zipCodes.size(); count++) {
			String zipCode = zipCodes.get(count);
			
			try {
				weatherService.getWeatherByZipCode(zipCode, false);
				this.logProgress(count, zipCodes.size(), "Load all OWM zip codes");
			} 
			catch (ServiceException e) {
				if(e.getErrorKey().equals("cityNotExists")) {					
					unknowZipCode.add(zipCode);
				}
				else {
					log.error("[Cron] Load all OWM zip codes error on "+zipCode+" :\n" + e.getErrorKey() + " : "+ e.getMessage());
					log.error("[Cron] Retry in 30 sec...");
					count--;
					try {
						Thread.sleep(30000);
					} catch (InterruptedException e1) {
						log.warn(e1.getMessage());
					}
				}
			}
		}
		log.info("[Cron] Load all OWM zip codes finished");
		
		// Envoi de mail automatique, seulement le mercredi
		List<User> users = userService.getAll();
		for (User user : users) {
			mailService.sendResultsMail(user);
		}
	}
}
