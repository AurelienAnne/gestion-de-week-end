package glitaa.service;

import glitaa.domain.City;
import glitaa.repository.CityRepository;
import glitaa.service.dto.CityDTO;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class CityService {

    private final CityRepository cityRepository;

    public CityService(CityRepository cityRepository) {
        this.cityRepository = cityRepository;
    }

    @Transactional(readOnly = true)
    public Page<CityDTO> getAll(Pageable pageable) {
        return cityRepository.findAll(pageable).map(CityDTO::new);
    }
    
    @Transactional(readOnly = true)
    public List<City> getAll() {
        return cityRepository.findAll();
    }
    
    @Transactional(readOnly = true)
    public List<String> getAllZipcode() {
        return cityRepository.findAllZipCodeDistinct();
    }
    
    public City findOne(long id) {
        return cityRepository.findOne(id);
    }
}
