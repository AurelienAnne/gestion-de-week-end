package glitaa.service;

import glitaa.service.dto.CityDTO;
import glitaa.service.dto.DepartmentDTO;
import glitaa.service.dto.LocationsDTO;
import glitaa.service.dto.RegionDTO;
import glitaa.service.util.ServiceCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class LocationsService {

    private final Logger log = LoggerFactory.getLogger(LocationsService.class);

    private final RegionService regionService;
    private final DepartmentService departmentService;
    private final CityService cityService;
    
    public LocationsService(RegionService regionService, CityService cityService, DepartmentService departmentService) {
        this.regionService = regionService;
        this.cityService = cityService;
        this.departmentService = departmentService;
    }

    /**
     * Retourne une liste de toutes les villes, départements et régions disponibles
     */
    public LocationsDTO getAllLocations() {   
    	// On lit depuis le cache
    	LocationsDTO locationsReturn = (LocationsDTO) ServiceCache.getInstance().get("locations.json", LocationsDTO.class);
    	if (locationsReturn != null) {
    		return locationsReturn;
    	}
    	
    	// S'il n'existe pas, on recherche dans la base de données 			
        locationsReturn = new LocationsDTO();

        for(RegionDTO region : regionService.getAll(new PageRequest(0, Integer.MAX_VALUE))) {
        	locationsReturn.addRegion(region);
        }
        
        for(CityDTO city : cityService.getAll(new PageRequest(0, Integer.MAX_VALUE))) {
        	locationsReturn.addCity(city);
        }
        
        for(DepartmentDTO department : departmentService.getAll(new PageRequest(0, Integer.MAX_VALUE))) {
        	locationsReturn.addDepartment(department);
        }
        
        // On enregistre en cache
        ServiceCache.getInstance().set("locations.json", locationsReturn);
        
        return locationsReturn;
    }
}
