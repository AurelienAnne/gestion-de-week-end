package glitaa.service.dto;

public class LocationDepartmentDTO extends LocationElementDTO {
	private String code;
	
	public LocationDepartmentDTO() {
		super();
	}

	public LocationDepartmentDTO(long id, String name, String code) {
		super(id, name);
		this.code = code;
	}

	public String getCode() {
		return (code.length() == 1) ? "0" + code : code;
	}

	public void setCode(String code) {
		this.code = code;
	}
}