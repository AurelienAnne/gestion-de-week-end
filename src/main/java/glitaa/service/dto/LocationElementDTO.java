package glitaa.service.dto;

public class LocationElementDTO {
	private long id;
	private String name;
	
	public LocationElementDTO() { }

	public LocationElementDTO(long id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}