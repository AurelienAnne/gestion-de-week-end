package glitaa.service.dto;

import java.util.ArrayList;
import glitaa.domain.City;
import glitaa.domain.Department;
import glitaa.domain.Region;

public class LocationsDTO {

	ArrayList<LocationCityDTO> cities = new ArrayList<>();
	ArrayList<LocationDepartmentDTO> departments = new ArrayList<>();
	ArrayList<LocationElementDTO> regions = new ArrayList<>();

	public LocationsDTO() { }
	
	public void addCity(CityDTO city) {
		this.cities.add(new LocationCityDTO(city.getId(), city.getCityName(), city.getZipCode()));
	}
	
	public void addCity(City city) {
		this.cities.add(new LocationCityDTO(city.getId(), city.getCityName(), city.getZipCode()));
	}
	
	public void addDepartment(DepartmentDTO department) {
		this.departments.add(new LocationDepartmentDTO(department.getId(), department.getDepartmentName(), department.getDepartmentCode()));
	}
	
	public void addDepartment(Department department) {
		this.departments.add(new LocationDepartmentDTO(department.getId(), department.getDepartmentName(), department.getDepartmentCode()));
	}
	
	public void addRegion(RegionDTO region) {
		this.regions.add(new LocationElementDTO(region.getId(), region.getName()));
	}
	
	public void addRegion(Region region) {
		this.regions.add(new LocationElementDTO(region.getId(), region.getName()));
	}
	
	public ArrayList<LocationCityDTO> getCities() {
		return cities;
	}

	public void setCities(ArrayList<LocationCityDTO> cities) {
		this.cities = cities;
	}

	public ArrayList<LocationDepartmentDTO> getDepartments() {
		return departments;
	}

	public void setDepartments(ArrayList<LocationDepartmentDTO> departments) {
		this.departments = departments;
	}

	public ArrayList<LocationElementDTO> getRegions() {
		return regions;
	}

	public void setRegions(ArrayList<LocationElementDTO> regions) {
		this.regions = regions;
	}
}
