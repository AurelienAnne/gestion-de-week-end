package glitaa.service.dto;

public class LocationCityDTO extends LocationElementDTO {
	private String zipCode;
	
	public LocationCityDTO() {
		super();
	}

	public LocationCityDTO(long id, String name, String zipCode) {
		super(id, name);
		this.zipCode = zipCode;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
}