package glitaa.service.dto;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import glitaa.domain.Department;
import glitaa.domain.Region;

public class RegionDTO {

	private Long id;
	private String name;
	private Set<Department> departments = new HashSet<>();
	
	public RegionDTO() { }
	
	public RegionDTO(Region region) {
		this(region.getId(), region.getName(), region.getDepartments());
	}
	
	public RegionDTO(Long id, String name, Set<Department> departments) {
		super();
		this.id = id;
		this.name = name;
		this.departments = departments;
	}

	@Id
	@GeneratedValue
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setnName(String name) {
		this.name = name;
	}	
	public Set<Department> getDepartments() {
		return departments;
	}
	public void setDepartments(Set<Department> departments) {
		this.departments = departments;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + " : " + getId() + " " + getName() + " " + getDepartments();
	}
}
