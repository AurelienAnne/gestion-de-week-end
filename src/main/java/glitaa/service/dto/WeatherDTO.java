package glitaa.service.dto;

import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.TimeZone;
import glitaa.service.dto.openweathermap.ListElement;
import glitaa.service.dto.openweathermap.Weather;
import glitaa.service.dto.openweathermap.Wind;

public class WeatherDTO {
    private Integer timestamp;   
    private Double temperature;
    private Double pressure;
    private Double sea_level;
    private Double grnd_level;
    private Double humidity;
    private ArrayList<Weather> weathers;
    private Integer clouds;
    private Double rain;
    private Double snow;
    private Wind wind;
    private String datetime;
    
    public WeatherDTO() { }
    
    public WeatherDTO(ListElement weatherReport) {
    	this.timestamp = weatherReport.getDt();
    	this.temperature = weatherReport.getMain().getTemp();
    	this.pressure = weatherReport.getMain().getPressure();
    	this.sea_level = weatherReport.getMain().getSea_level();
    	this.grnd_level = weatherReport.getMain().getGrnd_level();
    	this.humidity = weatherReport.getMain().getHumidity();
		this.weathers = weatherReport.getWeathers();
		this.clouds = weatherReport.getClouds().getAll();
		this.wind = weatherReport.getWind();
		if(weatherReport.getRain() != null) {
			this.rain = weatherReport.getRain().get3h();
		}
		if(weatherReport.getSnow() != null) {
			this.snow = weatherReport.getSnow().get3h();
		}
		
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		df.setTimeZone(TimeZone.getTimeZone("UTC"));
		this.datetime = df.format(new Date((long)this.timestamp*1000));
	}

	public Integer getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Integer timestamp) {
		this.timestamp = timestamp;
	}

	public Double getTemperature() {
		return temperature;
	}

	public void setTemperature(Double temperature) {
		this.temperature = temperature;
	}

	public Double getPressure() {
		return pressure;
	}

	public void setPressure(Double pressure) {
		this.pressure = pressure;
	}

	public Double getSea_level() {
		return sea_level;
	}

	public void setSea_level(Double sea_level) {
		this.sea_level = sea_level;
	}

	public Double getGrnd_level() {
		return grnd_level;
	}

	public void setGrnd_level(Double grnd_level) {
		this.grnd_level = grnd_level;
	}

	public Double getHumidity() {
		return humidity;
	}

	public void setHumidity(Double humidity) {
		this.humidity = humidity;
	}

	public ArrayList<Weather> getWeathers() {
		return weathers;
	}

	public void setWeathers(ArrayList<Weather> weathers) {
		this.weathers = weathers;
	}

	public Integer getClouds() {
		return clouds;
	}

	public void setClouds(Integer clouds) {
		this.clouds = clouds;
	}

	public Double getRain() {
		return rain;
	}

	public void setRain(Double rain) {
		this.rain = rain;
	}

	public Double getSnow() {
		return snow;
	}

	public void setSnow(Double snow) {
		this.snow = snow;
	}
	
	public Wind getWind() {
		return wind;
	}

	public void setWind(Wind wind) {
		this.wind = wind;
	}

	public String getDatetime() {
		return datetime;
	}

	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}	
}
