package glitaa.service.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class DayDTO {

	private Double noteMeteo;
	private Double noteFinale;
	private List<NoteSportDTO> notesSport;
	private DailyWeatherDTO dailyWeather;
	private List<WeatherDTO> weathers3h;
	private boolean saturday;

	public DayDTO() {
		super();
	}

	public DayDTO(Double noteMeteo, Double noteFinale, List<NoteSportDTO> notesSport, DailyWeatherDTO dailyWeather,
			List<WeatherDTO> weathers3h, boolean saturday) {
		super();
		this.noteMeteo = noteMeteo;
		this.noteFinale = noteFinale;
		this.notesSport = notesSport;
		this.dailyWeather = dailyWeather;
		this.weathers3h = weathers3h;
		this.saturday = saturday;
	}

	public DailyWeatherDTO getDailyWeather() {
		return dailyWeather;
	}

	public void setDailyWeather(DailyWeatherDTO dailyWeather) {
		this.dailyWeather = dailyWeather;
	}

	public List<WeatherDTO> getWeathers3h() {
		return weathers3h;
	}

	public void setWeathers3h(List<WeatherDTO> weathers3h) {
		this.weathers3h = weathers3h;
	}

	@JsonIgnore
	public boolean isSaturday() {
		return saturday;
	}

	public void setSaturday(boolean saturday) {
		this.saturday = saturday;
	}

	public Double getNoteMeteo() {
		return noteMeteo;
	}

	public void setNoteMeteo(Double noteMeteo) {
		this.noteMeteo = noteMeteo;
	}

	public Double getNoteFinale() {
		return noteFinale;
	}

	public void setNoteFinale(Double noteFinale) {
		this.noteFinale = noteFinale;
	}

	public List<NoteSportDTO> getNotesSport() {
		return notesSport;
	}

	public void setNotesSport(List<NoteSportDTO> notesSport) {
		this.notesSport = notesSport;
	}

	@Override
	public String toString() {
		return "noteMeteo: " + noteMeteo + ", noteFinale: " + noteFinale + ", saturday: " + saturday;
	}	
}
