package glitaa.service.dto;

import glitaa.domain.Sport;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.*;

public class SportDTO {

	private Long id;
	private String sportName;
	
	public SportDTO() {
        // Empty constructor needed for Jackson.
    }

    public SportDTO(Sport sport) {
        this(sport.getId(), sport.getSportName());
    }

    public SportDTO(Long id, String sportName) {
        this.id = id;
        this.sportName = sportName;      
    }

	@Id
    @GeneratedValue
    public Long getId() {
        return id;
    }
	public void setId(long id) {
		this.id = id;
	}
	
	@NotNull
	public String getSportName() {
		return sportName;
	}
	public void setSportName(String sportName) {
		this.sportName = sportName;
	}
	
	@Override
	public String toString() {
		return getClass().getSimpleName() + " : " + getId() + " " + getSportName();
	}
}
