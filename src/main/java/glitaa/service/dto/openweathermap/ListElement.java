package glitaa.service.dto.openweathermap;

import java.util.ArrayList;

public class ListElement {
	private Integer dt;
	private String dt_txt;
	private Main main;
	private ArrayList<Weather> weathers = new ArrayList<>();
	private Clouds clouds;
	private Wind wind;
	private Sys sys;
	private Rain rain;
	private Snow snow;

	public ListElement() {

	}

	public Integer getDt() {
		return dt;
	}

	public void setDt(Integer dt) {
		this.dt = dt;
	}

	public String getDt_txt() {
		return dt_txt;
	}

	public void setDt_txt(String dt_txt) {
		this.dt_txt = dt_txt;
	}

	public Main getMain() {
		return main;
	}

	public void setMain(Main main) {
		this.main = main;
	}

	public ArrayList<Weather> getWeathers() {
		return weathers;
	}

	public void setWeather(ArrayList<Weather> weathers) {
		this.weathers = weathers;
	}

	public Clouds getClouds() {
		return clouds;
	}

	public void setClouds(Clouds clouds) {
		this.clouds = clouds;
	}

	public Wind getWind() {
		return wind;
	}

	public void setWind(Wind wind) {
		this.wind = wind;
	}

	public Sys getSys() {
		return sys;
	}

	public void setSys(Sys sys) {
		this.sys = sys;
	}

	public Rain getRain() {
		return rain;
	}

	public void setRain(Rain rain) {
		this.rain = rain;
	}

	public Snow getSnow() {
		return snow;
	}

	public void setSnow(Snow snow) {
		this.snow = snow;
	}
}