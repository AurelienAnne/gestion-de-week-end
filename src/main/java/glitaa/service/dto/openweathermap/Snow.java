package glitaa.service.dto.openweathermap;

public class Snow {
	private Double h;

	public Double get3h() {
		return h;
	}

	public void set3h(Double h) {
		this.h = h;
	}
}