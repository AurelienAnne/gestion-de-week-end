package glitaa.service.dto.openweathermap;

public class Rain {
	private Double h;

	public Double get3h() {
		return h;
	}

	public void set3h(Double h) {
		this.h = h;
	}
}
