package glitaa.service.dto;

import java.util.Set;
import glitaa.domain.Sport;

public class AccountPreferencesDTO {

	LocationsDTO locations;
	Set<Sport> sports ;
	
	public AccountPreferencesDTO() { }
	
	public AccountPreferencesDTO(LocationsDTO locations, Set<Sport> sports) {
		super();
		this.locations = locations;
		this.sports = sports;
	}	

	public LocationsDTO getLocations() {
		return locations;
	}

	public void setLocations(LocationsDTO locations) {
		this.locations = locations;
	}

	public Set<Sport> getSports() {
		return sports;
	}

	public void setSports(Set<Sport> sports) {
		this.sports = sports;
	}
}
