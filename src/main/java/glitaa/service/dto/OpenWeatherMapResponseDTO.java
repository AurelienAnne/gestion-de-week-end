package glitaa.service.dto;

import java.util.ArrayList;
import java.util.List;
import glitaa.service.dto.openweathermap.City;
import glitaa.service.dto.openweathermap.ListElement;

public class OpenWeatherMapResponseDTO {
	private String cod;
	private Double message;
	private Double cnt;
	private List<ListElement> list = new ArrayList<>();
	private City city = new City();

	public List<ListElement> getList() {
		return list;
	}

	public void setList(ArrayList<ListElement> list) {
		this.list = list;
	}

	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	public String getCod() {
		return cod;
	}

	public void setCod(String cod) {
		this.cod = cod;
	}

	public Double getMessage() {
		return message;
	}

	public void setMessage(Double message) {
		this.message = message;
	}

	public Double getCnt() {
		return cnt;
	}

	public void setCnt(Double cnt) {
		this.cnt = cnt;
	}	
}

