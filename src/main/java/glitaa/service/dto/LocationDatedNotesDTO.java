package glitaa.service.dto;

public class LocationDatedNotesDTO {

	private CityDTO city;
	private Double note;
	private DayDTO sat;
	private DayDTO sun;

	public LocationDatedNotesDTO() {
		super();
	}

	public LocationDatedNotesDTO(CityDTO city, Double note, DayDTO sat, DayDTO sun) {
		super();
		this.city = city;
		this.note = note;
		this.sat = sat;
		this.sun = sun;
	}	

	public CityDTO getCity() {
		return city;
	}

	public void setCity(CityDTO city) {
		this.city = city;
	}

	public DayDTO getSat() {
		return sat;
	}

	public void setSat(DayDTO sat) {
		this.sat = sat;
	}

	public DayDTO getSun() {
		return sun;
	}

	public void setSun(DayDTO sun) {
		this.sun = sun;
	}

	public Double getNote() {
		return note;
	}

	public void setNote(Double note) {
		this.note = note;
	}
}
