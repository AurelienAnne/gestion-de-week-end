package glitaa.service.dto;

public class NoteSportDTO {

	private SportDTO sport;
	private Double note;
	
	public NoteSportDTO() {
		super();
	}

	public NoteSportDTO(SportDTO sport, Double note) {
		super();
		this.sport = sport;
		this.note = note;
	}

	public SportDTO getSport() {
		return sport;
	}

	public void setSport(SportDTO sport) {
		this.sport = sport;
	}

	public Double getNote() {
		return note;
	}

	public void setNote(Double note) {
		this.note = note;
	}
	
}
