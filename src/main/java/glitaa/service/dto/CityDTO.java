package glitaa.service.dto;

import javax.persistence.*;

import glitaa.domain.City;

public class CityDTO {
	private Long id;
	private String departmentCode;
	private String zipCode;
	private String cityName;
	private Integer population2012;
	private Double longitude;
	private Double latitude;
	
	public CityDTO() { }
	
	public CityDTO(City city) {
        this(city.getId(), city.getDepartmentCode(), city.getZipCode(), city.getCityName(), city.getPopulation2012(), city.getLongitude(),
        		city.getLatitude());
    }

	public CityDTO(Long id, String departmentCode, String zipCode, String cityName, Integer population2012, Double longitude, Double latitude) {
		super();
		this.id = id;
		this.departmentCode = departmentCode;
		this.zipCode = zipCode;
		this.cityName = cityName;
		this.population2012 = population2012;
		this.longitude = longitude;
		this.latitude = latitude;
	}

	@Id
	@GeneratedValue
    public Long getId() {
        return id;
    }
	public void setId(long id) {
		this.id = id;
	}

	public String getDepartmentCode() {
		return departmentCode;
	}

	public void setDepartmentCode(String departmentCode) {
		this.departmentCode = departmentCode;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public Integer getPopulation2012() {
		return population2012;
	}

	public void setPopulation2012(Integer population2012) {
		this.population2012 = population2012;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + " : " + getId() + " " + getCityName() + " " + getZipCode()
			+ " " + getPopulation2012() + " " + getLongitude() + " " + getLatitude();
	}
}
