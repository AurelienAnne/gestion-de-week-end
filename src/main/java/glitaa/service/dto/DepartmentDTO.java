package glitaa.service.dto;

import java.util.Set;
import javax.persistence.*;
import glitaa.domain.City;
import glitaa.domain.Department;
import glitaa.domain.Region;

public class DepartmentDTO {

	private Long id;
	private String departmentCode;
	//private Region region;
	private String departmentName;
	private Set<City> cities;
	
	public DepartmentDTO() { }
	
	public DepartmentDTO(Department department) {
		this(department.getId(), department.getDepartmentCode()/*, department.getRegion()*/, department.getDepartmentName(), department.getCities());
	}		

	public DepartmentDTO(Long id, String departmentCode/*, Region region*/, String departmentName,
			Set<City> cities) {
		super();
		this.id = id;
		this.departmentCode = departmentCode;
		//this.region = region;
		this.departmentName = departmentName;
		this.cities = cities;
	}

	@Id
	@GeneratedValue
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getDepartmentCode() {
		return departmentCode;
	}

	public void setDepartmentCode(String departmentCode) {
		this.departmentCode = departmentCode;
	}

	/*public Region getRegion() {
		return region;
	}

	public void setRegion(Region region) {
		this.region = region;
	}*/

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public Set<City> getCities() {
		return cities;
	}

	public void setCities(Set<City> cities) {
		this.cities = cities;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + " : " + getId() + " " + getDepartmentName() + " " + getCities();
	}
}
