package glitaa.service;

import glitaa.domain.City;
import glitaa.domain.Department;
import glitaa.repository.DepartmentRepository;
import glitaa.service.dto.DepartmentDTO;
import java.util.Set;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class DepartmentService {

    private final DepartmentRepository departmentRepository;

    public DepartmentService(DepartmentRepository departmentRepository) {
        this.departmentRepository = departmentRepository;
    }

    @Transactional(readOnly = true)
    public Page<DepartmentDTO> getAll(Pageable pageable) {
        return departmentRepository.findAll(pageable).map(DepartmentDTO::new);
    }
    
    public Set<City> getCitiesOf(Long departmentId) {
        return departmentRepository.getOne(departmentId).getCities();        
    }
    
    public Department getOne(Long departmentId) {
        return departmentRepository.getOne(departmentId);        
    }
}
