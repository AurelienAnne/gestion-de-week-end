package glitaa.service.util;

public class ServiceException extends Throwable {
	private static final long serialVersionUID = 1L;
	
	private Integer httpCode;
	private String errorKey;
	private String message;
	
	public ServiceException(Integer httpCode) {
		this.httpCode = httpCode;
	}
	
	public ServiceException(Integer httpCode, String errorKey) {
		this.httpCode = httpCode;
	}
	
	public ServiceException(Integer httpCode, String errorKey, String message) {
		this.httpCode = httpCode;
		this.errorKey = errorKey;
		this.message = message;
	}
	
	public Integer getHttpCode() {
		return httpCode;
	}
	public String getErrorKey() {
		return errorKey;
	}
	public String getMessage() {
		return message;
	}	
	
	public String toString() {
		StringBuilder stb = new StringBuilder();
		stb.append(httpCode.toString());
		
		if(errorKey != null) {
			stb.append(" " + errorKey);
		}
		if(message != null) {
			stb.append(" : " + message);
		}
		return stb.toString();
	}
}

