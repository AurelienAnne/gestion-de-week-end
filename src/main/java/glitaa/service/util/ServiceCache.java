package glitaa.service.util;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.sql.Timestamp;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class ServiceCache {	
	private static ServiceCache serviceCache;
	
	public static ServiceCache getInstance() {
		if(serviceCache == null) {
			serviceCache = new ServiceCache();
		}
		return serviceCache;
	}

	@Value("${cache}")
    private String cacheDir = "./cache";    
	
    private final Logger log = LoggerFactory.getLogger(ServiceCache.class);
	private ObjectMapper mapper;
		
	public ServiceCache() {
		this.mapper = new ObjectMapper();
	}
	
	private String getPathStr(String cacheFilepath) {
		return this.cacheDir + '/' + cacheFilepath;
	}
	
	private Path getPath(String cacheFilepath) {
		return Paths.get(this.getPathStr(cacheFilepath));
	}

	public void set(String cacheFilepath, Object object) {		
		try {
			// Si les répertoires de destination n'existe pas, on les créés
			if(cacheFilepath.contains("/")) {
				File directory = new File(this.getPathStr(cacheFilepath.substring(0, cacheFilepath.lastIndexOf("/"))));
			    if (!directory.exists()) {
			    	directory.mkdirs();
			    }
			}	
 		
 			this.mapper.writeValue(new File(this.getPathStr(cacheFilepath)), object);
 		} catch (Exception e) {
 			log.error("Erreur lors de l'enregistrement du cache " + this.getPathStr(cacheFilepath) + " :\n" + e.getMessage());
 		}
	}

	public <T> T get(String cacheFilepath, Class<T> typeReference) {
		try {
 			return this.mapper.readValue(new File(this.getPathStr(cacheFilepath)), typeReference);
 		} catch (Exception e) {
 			log.info("Pas de cache utilisé (" + e.getMessage() + ") : " + this.getPathStr(cacheFilepath));
 		}
		return null;
	}
	
	public <T> List<T> get(String cacheFilepath, TypeReference<List<T>> typeReference) {		
		try {
 			return this.mapper.readValue(new File(this.getPathStr(cacheFilepath)), typeReference);
 		} catch (Exception e) {
 			log.info("Pas de cache utilisé (" + e.getMessage() + ") : " + this.getPathStr(cacheFilepath));
 		}
		return null;
	}
	
	/*public <T> List<T> get(String cacheFilepath, TypeReference<List<T>> typeReference, Integer maxWriteSeconds) {		
		Long accessTimeTimestamp = null;
		try {
			accessTimeTimestamp = Files.readAttributes(this.getPath(cacheFilepath), BasicFileAttributes.class).lastAccessTime().toMillis();
		} catch (IOException e) {
			log.info("Pas de cache utilisé (" + e.getMessage() + ") : " + this.getPathStr(cacheFilepath));
			return null;
		}
		Long accessTimeSeconds = (new Timestamp(System.currentTimeMillis()).getTime() - accessTimeTimestamp) / 1000;
		
		if(accessTimeSeconds > maxWriteSeconds) {
			log.info("Cache trop ancien ("+accessTimeSeconds+">" + maxWriteSeconds + "s) : " + this.getPathStr(cacheFilepath));
			return null;
		}
		
		return this.get(cacheFilepath, typeReference);
	}*/
}

