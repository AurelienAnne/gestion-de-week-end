package glitaa.web.rest;

import com.codahale.metrics.annotation.Timed;
import glitaa.repository.RegionRepository;
import glitaa.service.RegionService;
import glitaa.service.dto.RegionDTO;
import glitaa.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiOperation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/api")
public class RegionResource {

    private final Logger log = LoggerFactory.getLogger(RegionResource.class);

    private static final String ENTITY_NAME = "regionManagement";

    private final RegionRepository regionRepository;

    private final RegionService regionService;

    public RegionResource(RegionRepository regionRepository, RegionService regionService) {

        this.regionRepository = regionRepository;
        this.regionService = regionService;
    }

    /**
     * GET  /cities : get all regions.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and with body all regions
     */
    @GetMapping("/regions")
    @Timed
    @ApiOperation(
		value = "Récupérer toutes les régions"
	)
    public ResponseEntity<List<RegionDTO>> getAllRegions(@ApiParam Pageable pageable) {
        final Page<RegionDTO> page = regionService.getAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/regions");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }
}
