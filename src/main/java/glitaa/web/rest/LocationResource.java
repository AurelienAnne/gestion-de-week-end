package glitaa.web.rest;

import com.codahale.metrics.annotation.Timed;
import glitaa.service.LocationsService;
import glitaa.service.dto.LocationsDTO;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class LocationResource {
    
	private final Logger log = LoggerFactory.getLogger(LocationResource.class);

    private static final String ENTITY_NAME = "locationsManagement";
    		
    private final LocationsService locationsService;
    
    public LocationResource(LocationsService locationsService) {
		this.locationsService = locationsService;
	}
    
    /**
     * GET  /locations : get all locations (region, departments, cities).
     */
    @GetMapping("/locations")
    @Timed
    @ApiOperation(
		value = "Récupérer l'ensemble des noms des régions, départemnts et villes"
	)
    public ResponseEntity<LocationsDTO> getAllLocations() {
        return new ResponseEntity<>(locationsService.getAllLocations(), HttpStatus.OK);
    }
}

