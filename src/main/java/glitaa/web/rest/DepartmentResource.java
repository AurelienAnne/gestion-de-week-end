package glitaa.web.rest;

import com.codahale.metrics.annotation.Timed;
import glitaa.repository.DepartmentRepository;
import glitaa.service.DepartmentService;
import glitaa.service.dto.DepartmentDTO;
import glitaa.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiOperation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/api")
public class DepartmentResource {

    private final Logger log = LoggerFactory.getLogger(DepartmentResource.class);

    private static final String ENTITY_NAME = "departmentManagement";

    private final DepartmentRepository departmentRepository;

    private final DepartmentService departmentService;

    public DepartmentResource(DepartmentRepository departmentRepository, DepartmentService departmentService) {

        this.departmentRepository = departmentRepository;
        this.departmentService = departmentService;
    }

    /**
     * GET  /departments : get all departments.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and with body all departments
     */
    @GetMapping("/departments")
    @Timed
    @ApiOperation(
		value = "Récupérer tous les départements"
	)
    public ResponseEntity<List<DepartmentDTO>> getAllDepartments(@ApiParam Pageable pageable) {
        final Page<DepartmentDTO> page = departmentService.getAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/departments");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }
}
