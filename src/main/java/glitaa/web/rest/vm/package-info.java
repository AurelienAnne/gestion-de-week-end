/**
 * View Models used by Spring MVC REST controllers.
 */
package glitaa.web.rest.vm;
