package glitaa.web.rest;

import com.codahale.metrics.annotation.Timed;

import glitaa.service.MailService;
import glitaa.service.UserService;
import glitaa.service.WeatherService;
import glitaa.service.dto.DailyWeatherDTO;
import glitaa.service.dto.DayDTO;
import glitaa.service.dto.LocationDatedNotesDTO;
import glitaa.service.dto.WeatherDTO;
import glitaa.service.util.ServiceException;
import glitaa.web.rest.util.HeaderUtil;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.*;
import javax.validation.Valid;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/api/weather/")
public class WeatherResource {

	private final Logger log = LoggerFactory.getLogger(WeatherResource.class);

	private static final String ENTITY_NAME = "weatherManagement";

	private final WeatherService weatherService;
	private final UserService userService;

	public WeatherResource(WeatherService weatherService, UserService userService) {
		this.weatherService = weatherService;
		this.userService = userService;
	}

	@GetMapping("/city/{zipCode}")
	@Timed
	@ApiOperation(
			value = "Retourne la dernière météo d'une ville d'après son code postal"
			)
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Les données météo de la ville sont bien retournées"),
			@ApiResponse(code = 400, message = "Le code postal n'est pas valide"),
			@ApiResponse(code = 404, message = "La ville n'existe pas"),
			@ApiResponse(code = 500, message = "Erreur du serveur") 
	})  
	public ResponseEntity<List<WeatherDTO>> getWeather(@Valid @PathVariable String zipCode) {
		List<WeatherDTO> weather;

		try {
			weather = weatherService.getWeatherByZipCode(zipCode, true);
		}
		catch(ServiceException e) {    		
			log.error("Erreur WeatherService : " + e);

			return ResponseEntity.status(e.getHttpCode())
					.headers(HeaderUtil.createFailureAlert(ENTITY_NAME, e.getErrorKey(), e.getMessage()))
					.body(null);
		}

		return new ResponseEntity<>(weather, HttpStatus.OK);
	}

	@GetMapping("/city/{zipCode}/daily")
	@Timed
	@ApiOperation(
			value = "Retourne la météo par jour d'une ville d'après son code postal"
			)
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Les données météo journalières de la ville est bien retournée"),
			@ApiResponse(code = 400, message = "Le code postal n'est pas valide"),
			@ApiResponse(code = 404, message = "La ville n'existe pas"),
			@ApiResponse(code = 500, message = "Erreur du serveur") 
	})  
	public ResponseEntity<List<DailyWeatherDTO>> getDailyWeather(@Valid @PathVariable String zipCode) {
		List<DailyWeatherDTO> dailyWeather;

		try {
			dailyWeather = weatherService.getDailyWeatherByZipCode(zipCode);
		}
		catch(ServiceException e) {    		
			log.error("Erreur WeatherService : " + e);

			return ResponseEntity.status(e.getHttpCode())
					.headers(HeaderUtil.createFailureAlert(ENTITY_NAME, e.getErrorKey(), e.getMessage()))
					.body(null);
		}

		return new ResponseEntity<>(dailyWeather, HttpStatus.OK);
	}

	@GetMapping("/city/{zipCode}/evaluation")
	@Timed
	@ApiOperation(
			value = "Retourne la note météo d'une ville d'après son code postal"
			)
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "La note météo de la ville est bien retournée"),
			@ApiResponse(code = 400, message = "Le code postal n'est pas valide"),
			@ApiResponse(code = 404, message = "La ville n'existe pas"),
			@ApiResponse(code = 500, message = "Erreur du serveur") 
	})  
	public ResponseEntity<List<DayDTO>> getWeatherEvaluation(@Valid @PathVariable String zipCode) {
		List<DayDTO> notes;

		try {
			notes = weatherService.getWeatherEvaluationByZipCode(zipCode);
		}
		catch(ServiceException e) {    		
			log.error("Erreur WeatherService : " + e);

			return ResponseEntity.status(e.getHttpCode())
					.headers(HeaderUtil.createFailureAlert(ENTITY_NAME, e.getErrorKey(), e.getMessage()))
					.body(null);
		}

		return new ResponseEntity<>(notes, HttpStatus.OK);
	}

	@GetMapping("/allnotes")
	@Timed
	@ApiOperation(
			value = "Retourne les notes météo de toutes les villes"
			)
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Les notes sont bien retournées"),
			@ApiResponse(code = 500, message = "Erreur du serveur") 
	})  
	public ResponseEntity<List<LocationDatedNotesDTO>> getAllWeatherNotes() {
		List<LocationDatedNotesDTO> weatherNotes = new ArrayList<>();

		try {
			weatherNotes = weatherService.getAllWeatherEvaluations(userService.getUserWithAuthorities());
		}
		catch(ServiceException e) {    		
			log.error("Erreur WeatherService : " + e);

			return ResponseEntity.status(e.getHttpCode())
					.headers(HeaderUtil.createFailureAlert(ENTITY_NAME, e.getErrorKey(), e.getMessage()))
					.body(null);
		}
		
		return new ResponseEntity<>(weatherNotes, HttpStatus.OK);
	}
	
	@GetMapping("/besttennotes")
	@Timed
	@ApiOperation(
			value = "Retourne les dix meilleures notes météo de toutes les villes"
			)
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Les notes sont bien retournées"),
			@ApiResponse(code = 500, message = "Erreur du serveur") 
	})  
	public ResponseEntity<List<LocationDatedNotesDTO>> getBestTenNotes() {
		List<LocationDatedNotesDTO> bestWeatherNotes = new ArrayList<>();

		try {
			bestWeatherNotes = weatherService.getBestNotes(10, userService.getUserWithAuthorities());
		}
		catch(ServiceException e) {    		
			log.error("Erreur WeatherService : " + e);

			return ResponseEntity.status(e.getHttpCode())
					.headers(HeaderUtil.createFailureAlert(ENTITY_NAME, e.getErrorKey(), e.getMessage()))
					.body(null);
		}
		
		return new ResponseEntity<>(bestWeatherNotes, HttpStatus.OK);
	}
}
