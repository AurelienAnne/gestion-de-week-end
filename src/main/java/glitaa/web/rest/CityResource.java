package glitaa.web.rest;

import com.codahale.metrics.annotation.Timed;
import glitaa.repository.CityRepository;
import glitaa.service.CityService;
import glitaa.service.dto.CityDTO;
import glitaa.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiOperation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/api")
public class CityResource {

    private final Logger log = LoggerFactory.getLogger(CityResource.class);

    private static final String ENTITY_NAME = "cityManagement";

    private final CityRepository cityRepository;

    private final CityService cityService;

    public CityResource(CityRepository cityRepository, CityService cityService) {

        this.cityRepository = cityRepository;
        this.cityService = cityService;
    }

    /**
     * GET  /cities : get all cities.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and with body all cities
     */
    @GetMapping("/cities")
    @Timed
    @ApiOperation(
		value = "Récupérer toutes les villes"
	)
    public ResponseEntity<List<CityDTO>> getAllCities(@ApiParam Pageable pageable) {
        final Page<CityDTO> page = cityService.getAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/cities");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }
}
