package glitaa.repository;

import glitaa.domain.City;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface CityRepository extends JpaRepository<City, Long> {

	@Query("SELECT DISTINCT(zipCode) FROM City")
	List<String> findAllZipCodeDistinct();
}
