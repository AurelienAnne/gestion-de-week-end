import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Sport } from './sport.model';
import { SportPopupService } from './sport-popup.service';
import { SportService } from './sport.service';

@Component({
    selector: 'jhi-sport-dialog',
    templateUrl: './sport-dialog.component.html'
})
export class SportDialogComponent implements OnInit {

    sport: Sport;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private sportService: SportService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.sport.id !== undefined) {
            this.subscribeToSaveResponse(
                this.sportService.update(this.sport));
        } else {
            this.subscribeToSaveResponse(
                this.sportService.create(this.sport));
        }
    }

    private subscribeToSaveResponse(result: Observable<Sport>) {
        result.subscribe((res: Sport) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: Sport) {
        this.eventManager.broadcast({ name: 'sportListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-sport-popup',
    template: ''
})
export class SportPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private sportPopupService: SportPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.sportPopupService
                    .open(SportDialogComponent as Component, params['id']);
            } else {
                this.sportPopupService
                    .open(SportDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
