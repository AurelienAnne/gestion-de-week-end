import { BaseEntity } from './../../shared';

export class Sport implements BaseEntity {
    constructor(
        public id?: number,
        public sportName?: string,
    ) {
    }
}
