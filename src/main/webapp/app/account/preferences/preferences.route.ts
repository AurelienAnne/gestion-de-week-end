import { Route } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { PreferencesComponent } from './preferences.component';

export const preferencesRoute: Route = {
    path: 'preferences',
    component: PreferencesComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'Preferences'
    },
    canActivate: [UserRouteAccessService]
};
