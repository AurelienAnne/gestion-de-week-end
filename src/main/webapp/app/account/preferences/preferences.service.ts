import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';
import { Http } from '@angular/http';

@Injectable()
export class PreferencesService {

    constructor(private http: Http) {}

    save(locations: any, sports: any): Observable<any> {
        return this.http.put(SERVER_API_URL + 'api/account/preferences', {
            'locations': locations,
            'sports': sports
        });
    }

    get(): Observable<any> {
        return this.http.get(SERVER_API_URL + 'api/account/preferences');
    }
}
