import { Component, OnInit } from '@angular/core';
import { Principal, LocationService } from '../../shared';
import { SportService } from '../../entities/sport/sport.service';
import { PreferencesService } from './preferences.service'

@Component({
    selector: 'jhi-preferences',
    templateUrl: './preferences.component.html'
})
export class PreferencesComponent implements OnInit {
    error: string;
    success: string;
    preferencesAccount: any;
    languages: any[];
    saveInProgress = false;

    locationList: {
        'cities': Object[],
        'regions': Object[],
        'departments': Object[]
    };

    locationSelection: {
        'cities': Object[],
        'regions': Object[],
        'departments': Object[]
    };
    locationQuery = '';
    locationFilteredList: {
        'cities': Object[],
        'regions': Object[],
        'departments': Object[]
    };

    sportList: Object[];
    sportSelection = [];
    sportQuery = '';
    sportFilteredList = [];
    sportNoQueryFilteredList = [];

    constructor(
        private locationService: LocationService,
        private sportService: SportService,
        private preferencesService: PreferencesService,
        private principal: Principal
    ) {
        this.locationSelection = {
            'cities': [],
            'regions': [],
            'departments': []
        }

        this.locationFilteredList = {
            'cities': [],
            'regions': [],
            'departments': []
        };
    }

    ngOnInit() {
        this.principal.identity().then((account) => {
            this.preferencesAccount = this.copyAccount(account);
        });

        // On récupère déjà les préférences enregistrées de l'utilisateur
        this.preferencesService.get().subscribe((preferences) => {
            preferences = preferences.json();

            // En parrallèle, on traite les locations et les sports
            this.locationService.get(function(locations) {
                this.locationList = locations;

                if (preferences.locations !== null) {
                    preferences.locations.cities.forEach((city) => {
                        this.selectLocation('cities', city);
                    });
                    preferences.locations.departments.forEach((department) => {
                        this.selectLocation('departments', department);
                    });
                    preferences.locations.regions.forEach((region) => {
                        this.selectLocation('regions', region);
                    });
                }
            }.bind(this));

            this.sportService.query().subscribe((sports) => {
                this.sportList = sports.json;
                this.sportFilteredList = sports.json;
                this.sportNoQueryFilteredList = sports.json;

                if (preferences.sports !== null) {
                    preferences.sports.forEach((sport) => {
                        this.selectSport(sport);
                    });
                }
            });
        }, () => {
            this.success = null;
            this.error = 'Impossible de récupèrer les préférences';
        });
    }

    locationFilter() {
        this.success = null;
        this.locationFilteredList = {
            'cities': [],
            'regions': [],
            'departments': []
        };

        if (this.locationQuery.length >= 2) {
            this.locationList.cities.forEach(function(location) {
                if (this.locationQuery.toLowerCase().includes(location.name.toLowerCase().substring(0, this.locationQuery.length))
                    || this.locationQuery.includes(location.zipCode.substring(0, this.locationQuery.length))) {
                    this.locationFilteredList.cities.push(location);
                }
            }.bind(this));

            this.locationList.departments.forEach(function(location) {
                if (location.name.toLowerCase().includes(this.locationQuery.toLowerCase()) || this.locationQuery.substring(0, 2) === location.code) {
                    this.locationFilteredList.departments.push(location);
                }
            }.bind(this));

            this.locationList.regions.forEach(function(location) {
                if (location.name.toLowerCase().includes(this.locationQuery.toLowerCase())) {
                    this.locationFilteredList.regions.push(location);
                }
            }.bind(this));
        }
    }

    sportFilter() {
        this.success = null;
        if (this.sportQuery.length >= 1) {
            this.sportFilteredList = [];
            this.sportList.forEach(function(sport) {
                if (sport.sportName.toLowerCase().includes(this.sportQuery.toLowerCase())) {
                    this.sportFilteredList.push(Object.create(sport));
                }
            }.bind(this));
        } else {
            this.sportFilteredList = this.sportNoQueryFilteredList;
        }
    }

    selectLocation(locationType: string, item) {
        this.locationSelection[locationType].push(item);
        this.locationQuery = '';
        this.locationFilteredList = {
            'cities': [],
            'regions': [],
            'departments': []
        };
    }

    selectSport(item) {
        this.sportSelection.push(item);
        this.sportNoQueryFilteredList.splice(this.sportNoQueryFilteredList.indexOf(item), 1);
        this.sportQuery = '';
        this.sportFilteredList = this.sportNoQueryFilteredList;
    }

    removeLocation(locationType: string, item) {
        this.locationSelection[locationType].splice(this.locationSelection[locationType].indexOf(item), 1);
    }

    removeSport(item) {
        this.sportSelection.splice(this.sportSelection.indexOf(item), 1);
        this.sportNoQueryFilteredList.push(item);
        this.sportFilter();
    }

    /*
        // TODO: Problème : Des éléments dans sportList se retrouve supprimé, sans doute un problème de référence
        resetSportSelection() {
        this.sportSelection = [];
        this.sportFilteredList = this.sportList;
        this.sportNoQueryFilteredList = this.sportList;
        console.log(this.sportList);
        this.sportFilter();
    }*/

    copyAccount(account) {
        return {
            activated: account.activated,
            email: account.email,
            firstName: account.firstName,
            langKey: account.langKey,
            lastName: account.lastName,
            login: account.login,
            imageUrl: account.imageUrl
        };
    }

    savePreferences() {
        this.saveInProgress = true;
        this.preferencesService.save(this.locationSelection, this.sportSelection).subscribe(() => {
            this.error = null;
            this.success = 'OK';
            this.saveInProgress = false;
        }, () => {
            this.success = null;
            this.error = 'ERROR';
            this.saveInProgress = false;
        });
    }
}
