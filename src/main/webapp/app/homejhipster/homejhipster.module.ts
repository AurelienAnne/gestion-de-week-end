import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ProjetglitaaSharedModule } from '../shared';

import { HOME_JHIPSTER_ROUTE, HomeJhipsterComponent } from './';

@NgModule({
    imports: [
        ProjetglitaaSharedModule,
        RouterModule.forRoot([ HOME_JHIPSTER_ROUTE ], { useHash: true })
    ],
    declarations: [
        HomeJhipsterComponent,
    ],
    entryComponents: [
    ],
    providers: [
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ProjetglitaaHomeJhipsterModule {}
