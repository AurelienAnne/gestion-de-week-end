import { Route } from '@angular/router';

import { UserRouteAccessService } from '../shared';
import { HomeJhipsterComponent } from './';

export const HOME_JHIPSTER_ROUTE: Route = {
    path: 'homejhipster',
    component: HomeJhipsterComponent,
    data: {
        authorities: [],
        pageTitle: 'Welcome, Java Hipster!'
    }
};
