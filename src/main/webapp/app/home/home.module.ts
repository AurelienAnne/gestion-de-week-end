import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MomentModule } from 'angular2-moment';

import { ProjetglitaaSharedModule } from '../shared';

import { HOME_ROUTE, HomeComponent } from './';

import { CityWeathersComponent } from './weather/cityweathers.component';
import { DayWeatherComponent } from './weather/dayweather.component';

@NgModule({
    imports: [
        ProjetglitaaSharedModule,
        RouterModule.forRoot([ HOME_ROUTE ], { useHash: true }),
        MomentModule
    ],
    declarations: [
        HomeComponent,
        CityWeathersComponent,
        DayWeatherComponent
    ],
    entryComponents: [
    ],
    providers: [
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ProjetglitaaHomeModule {}
