import { Component, OnInit, Input } from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';
import { BrowserModule } from '@angular/platform-browser';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

@Component({
    selector: 'jhi-city-weathers',
    templateUrl: './cityweathers.component.html',
})
export class CityWeathersComponent {
    @Input('weathers')
    weathers: {
        sat: object,
        sun: object
    };

    dateSat: Date;
    dateSun: Date;

    constructor() {
        this.dateSat = new Date();
        this.dateSun = new Date();
        this.dateSat.setDate(this.dateSat.getDate() + (6 + 7 - this.dateSat.getDay()) % 7);
        this.dateSun.setDate(this.dateSun.getDate() + (0 + 7 - this.dateSun.getDay()) % 7);
    }
}
