import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';
import { BrowserModule } from '@angular/platform-browser';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { MomentModule } from 'angular2-moment';
import 'moment/locale/fr';

import { Account, LoginModalService, Principal, WeatherService } from '../../shared';

@Component({
    selector: 'jhi-day-weather',
    templateUrl: './dayweather.component.html',
    styleUrls: [
        'dayweather.css'
    ]
})
export class DayWeatherComponent implements OnChanges {
    @Input('dayWeathers')
    dayWeathers: {
        notesSport: object[],
        noteFinale: Number,
        noteMeteo: Number
    };

    @Input('date')
    date: Date;

    Math: any;
    detailClick = false;
    dayWeatherGlobal: Object;

    constructor() {
        this.Math = Math;
    }

    ngOnChanges(changes: SimpleChanges): void {
        this.getDayWeatherGlobal();
    }

    getDayWeatherGlobal() {
        const weathers = [];
        const weathersCount = [];

        if (this.dayWeathers['weathers3h'].length === 0) { // Si on a aucun détail horaire des météos
            this.dayWeatherGlobal = {  // On crée une météo inconnue
                main: 'empty',
                description: 'Météo inconnue',
                icon: '03d'
            };
        } else {
            this.dayWeathers['weathers3h'].forEach((dayPeriodWeatherInfos) => {
                const dayPeriodWeather = dayPeriodWeatherInfos.weathers[0];
                if (dayPeriodWeather != null) {
                    let i = 0;
                    while (i < weathers.length && weathers[i].icon !== dayPeriodWeather.icon) {
                        i = i + 1;
                    }

                    if (i === weathers.length) {
                        weathers.push(dayPeriodWeather);
                        weathersCount.push(1);
                    } else {
                        weathersCount[i] = weathersCount[i] + 1;
                    }
                }
            });

            this.dayWeatherGlobal = weathers[weathersCount.indexOf(Math.max(...weathersCount))];
        }
    }
}
