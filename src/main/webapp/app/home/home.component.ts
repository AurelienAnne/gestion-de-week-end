import { Component, OnInit, Input } from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';
import { BrowserModule } from '@angular/platform-browser';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { MomentModule } from 'angular2-moment';
import 'moment/locale/fr';

import { Account, LoginModalService, Principal, WeatherService } from '../shared';

@Component({
    selector: 'jhi-home',
    templateUrl: './home.component.html',
    styleUrls: [
        'home.css'
    ]
})
export class HomeComponent implements OnInit {
    account: Account;
    modalRef: NgbModalRef;
    Math: any;
    citiesWeather: object[];
    dateSat: Date;
    dateSun: Date;
    error: String;
    loading = true;

    constructor(
        private principal: Principal,
        private loginModalService: LoginModalService,
        private eventManager: JhiEventManager,
        private weatherService: WeatherService
    ) {
        this.Math = Math;
    }

    ngOnInit() {
        this.dateSat = new Date();
        this.dateSun = new Date();
        const currentDate = new Date();
        if (currentDate.getDay() === 0) { // Sunday
            this.dateSat.setDate(this.dateSun.getDate() - 1);
        } else {
            this.dateSat.setDate(this.dateSat.getDate() + (6 + 7 - this.dateSat.getDay()) % 7); // Get next weekend
            this.dateSun.setDate(this.dateSat.getDate() + 1);
        }

        this.principal.identity().then((account) => {
            this.account = account;
            if (account != null) {
                this.getBestNotes();
            }
        });
        this.registerAuthenticationSuccess();
    }

    registerAuthenticationSuccess() {
        this.eventManager.subscribe('authenticationSuccess', (message) => {
            this.principal.identity().then((account) => {
                this.account = account;
                if (account != null) {
                    this.getBestNotes();
                }
            });
        });
    }

    getBestNotes() {
        this.weatherService.getResults().subscribe((json) => {
            this.citiesWeather = json;
            this.loading = false;
        },
        (err) => {
            this.error = 'Erreur lors de la récupération des prévisions.';
        })
    }

    isAuthenticated() {
        return this.principal.isAuthenticated();
    }

    login() {
        this.modalRef = this.loginModalService.open();
    }
}
