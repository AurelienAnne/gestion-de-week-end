import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

@Injectable()
export class WeatherService  {
    constructor(private http: Http) { }

    getResults(): Observable<any> {
        return this.http.get(SERVER_API_URL + 'api/weather/besttennotes').map((res: Response) => res.json());
    }
}
