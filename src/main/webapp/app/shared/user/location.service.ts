import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

@Injectable()
export class LocationService  {
    constructor(private http: Http) { }

    get(callback) {
        const req = new XMLHttpRequest();
        req.open('GET', SERVER_API_URL + 'api/locations');
        req.onload = () => {
            callback(JSON.parse(req.response));
        };
        req.send();
    }
}
