import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { DatePipe } from '@angular/common';

import {
    ProjetglitaaSharedLibsModule,
    ProjetglitaaSharedCommonModule,
    CSRFService,
    AuthServerProvider,
    AccountService,
    LocationService,
    UserService,
    StateStorageService,
    LoginService,
    LoginModalService,
    Principal,
    HasAnyAuthorityDirective,
    JhiLoginModalComponent,
    WeatherService
} from './';

@NgModule({
    imports: [
        ProjetglitaaSharedLibsModule,
        ProjetglitaaSharedCommonModule
    ],
    declarations: [
        JhiLoginModalComponent,
        HasAnyAuthorityDirective
    ],
    providers: [
        LoginService,
        LoginModalService,
        AccountService,
        LocationService,
        StateStorageService,
        Principal,
        CSRFService,
        AuthServerProvider,
        UserService,
        DatePipe,
        WeatherService
    ],
    entryComponents: [JhiLoginModalComponent],
    exports: [
        ProjetglitaaSharedCommonModule,
        JhiLoginModalComponent,
        HasAnyAuthorityDirective,
        DatePipe
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]

})
export class ProjetglitaaSharedModule {}
